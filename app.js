const express = require('express');
var indexRouter = require('./routes/index.js')
var guestbookRouter = require('./routes/guestbook.js')
var signupRouter = require('./routes/signup.js')
const cookieParser = require('cookie-parser')


var path = require('path');
require('dotenv').config()
    

const port = process.env.PORT || 3000



var app = express();
app.set('views', 'views');
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({extended: true}))
app.use(express.static(path.join(__dirname, 'public')));

app.use(cookieParser())

app.use('/', indexRouter)
app.use('/guestbook', guestbookRouter)
app.use('/signup', signupRouter)








app.listen(port);
