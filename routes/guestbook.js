const express = require('express');
const router = express.Router();
const database = require('../db/index');



let vulnerabilityON = 1

router.get('/', async (req, res) => {
   
    res.setHeader('set-cookie', "foo=bar")

    let guests = []
    try {
     guests =  (await database.query('SELECT * FROM guestbook')).rows
        
    } catch (error) {
        
    }


    //console.log(req.originalUrl)
    console.log(vulnerabilityON)
    res.render('guestbook', {vulnerabilityON: vulnerabilityON, linkActive: "Guestbook", guests: guests})



})

router.post('/',async (req,res) =>{

    try {
        await database.query('INSERT INTO guestbook (Name, Message) VALUES ($1, $2)', [req.body.name, req.body.message])
        
    } catch (error) {
        
    }


    res.redirect(req.originalUrl)

  
})

router.post('/vulnerability', async (req,res) => {
    vulnerabilityON = req.body.ranjivost
    res.redirect("/guestbook")


})

module.exports = router;